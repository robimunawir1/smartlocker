#include <Wire.h>
#include <SoftwareSerial.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_Thermal.h>
#include <PCF8574.h>
#include <PN532_SWHSU.h>
#include <PN532.h>
#include <Ethernet.h>

SoftwareSerial SWSerial0( 9, 6 ); // RX, TX
PN532_SWHSU pn532swhsu( SWSerial0 );
PN532 nfc( pn532swhsu ); 

SoftwareSerial SWSerial1( 4, 5 ); // RX, TX
Adafruit_Thermal printer(&SWSerial1); 

LiquidCrystal_I2C lcd(0x27, 16, 2); 
PCF8574 PCF_01(0x23);
PCF8574 PCF_02(0x21);

IPAddress ip(192, 168, 1, 161);
EthernetClient client;
IPAddress server(192, 168, 1, 150);
byte mac[] = { 0xDD, 0xCD, 0xAE, 0x0F, 0xFE, 0xDB };

void setup() {
  Serial.begin(9600);
  
  lcd.init();  
  lcd.backlight(); 
  lcd.print("SMARTLOCKER V1.0"); 
  lcd.setCursor(0,1);
  lcd.print("  OPENLIBRARY"); 
  
  PCF_01.begin();
  PCF_02.begin();

  nfc.begin();
  uint32_t versiondata = nfc.getFirmwareVersion();
  nfc.SAMConfig();
  if (! versiondata) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("MODUL RFID NOK");
    while (1); // halt
  }
  
  Ethernet.begin(mac, ip);
  if (client.connect(server, 6123)) {
    ip = Ethernet.localIP();
    lcd.clear();
    for (byte thisByte = 0; thisByte < 4; thisByte++) {
      lcd.print(ip[thisByte], DEC);
      if(thisByte < 3)lcd.print(".");
    }
    
    lcd.setCursor(0, 1);
    for (byte thisByte = 0; thisByte < 6; thisByte++) {
      lcd.print(mac[thisByte], HEX);
    }
  } else {
    lcd.clear();
    lcd.print("KONEKSI PUTUS");
    lcd.setCursor(0,1);
    ip = Ethernet.localIP();
    for (byte thisByte = 0; thisByte < 4; thisByte++) {
      lcd.print(ip[thisByte], DEC);
      if(thisByte < 3)lcd.print(".");
    }
    while(1);
  }
    
  SWSerial1.begin(9600);
  printer.begin();
  printer.feed(5);         
  print_ticket("PRINTER READY!");
}

void loop() {
    boolean success;
    uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  
    uint8_t uidLength;                       
    success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength);
    if(success){    
      lcd.clear();
      lcd.print("CARD DETECTED!");
      lcd.setCursor(0,1);
      lcd.print("UID:" + toString(uid, uidLength));
      for (uint8_t i=0; i < uidLength; i++){
          if (uid[i] <= 0xF){
            client.print(F("0"));
          }
          client.print(uid[i], HEX);
      }
      delay(1000);
    } else {
      lcd.clear();
      lcd.print("SMARTLOCKER V1.0"); 
      lcd.setCursor(0,1);
      lcd.print("  OPENLIBRARY"); 
    }
    process();
}

void process(void){
  if(client.available()){
        char number = client.read();
        delay(500);
        switch(number){
          case '1' : print_ticket("Loker 1"); open_locker_1(0);  break;
          case '2' : print_ticket("Loker 2"); open_locker_1(1);  break;
          case '3' : print_ticket("Loker 3"); open_locker_1(2);  break;
          case '4' : print_ticket("Loker 4"); open_locker_1(3);  break;
          case '5' : print_ticket("Loker 5"); open_locker_1(4);  break;
          case '6' : print_ticket("Loker 6"); open_locker_1(5);  break;
          case '7' : print_ticket("Loker 7"); open_locker_1(6);  break;
          case '8' : print_ticket("Loker 8"); open_locker_1(7);  break;
          case '9' : print_ticket("Loker 9"); open_locker_2(0);  break;
          case '10': print_ticket("Loker 10"); open_locker_2(1);  break;
          case '11': print_ticket("Loker 11"); open_locker_2(2);  break;
          case '12': print_ticket("Loker 12"); open_locker_2(3);  break;
          default  : locker_full(); break;
        }
      }
}

void print_ticket(String x){
  printer.justify('C');  
  printer.setSize('L');
  printer.doubleHeightOn();
  printer.boldOn();
  printer.println(x);
  printer.feed(5);
}

void locker_full(void){
  lcd.clear();
  lcd.print("SMARTLOCKER V1.0"); 
  lcd.setCursor(0,1);
  lcd.print(" FULL CAPACITY!");
}

void open_locker_1(int x){
  PCF_01.write(x, LOW);
  delay(5000);
  PCF_01.write(x, HIGH);
}

void open_locker_2(int x){
  PCF_02.write(x, LOW);
  delay(5000);
  PCF_02.write(x, HIGH);
}

String toString(uint8_t *data, uint8_t numBytes){
  String cardId;
  for (uint8_t i = 0; i < numBytes; i++) {
      cardId = cardId + String(data[i], HEX);
  }
  return cardId;
}

